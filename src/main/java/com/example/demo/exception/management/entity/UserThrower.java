package com.example.demo.exception.management.entity;

import com.example.demo.entity.User;
import com.example.demo.exception.customexception.UserException;
import com.example.demo.validation.entity.UserValidation;

import java.util.Optional;

public class UserThrower {

    public static void checkEmailIsValid(String email) {

        if (!UserValidation.emailIsValid.test(email)) {
            throw new UserException("User email is not valid");
        }
    }

    public static void checkUserIsExist(Optional<User> user) {

        if (!user.isPresent()) {
            throw new UserException("Given user not found");
        }
    }

    public static void checkUserIsExistForRegistration(Optional<User> user) {

        if (user.isPresent()) {
            throw new UserException("Given email address is already using");
        }
    }
}
