package com.example.demo.service;

import com.example.demo.dto.request.RegisterRequest;
import com.example.demo.dto.response.UserResponse;
import com.example.demo.entity.User;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.impl.UserServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    @InjectMocks
    UserServiceImpl userService;

    @Mock
    BCryptPasswordEncoder passwordEncoder;

    @Mock
    UserRepository userRepository;

    private final static  String EMAIL = "mujde.akif@gmail.com";
    private final static  String PASSWORD = "1";

    @Test
    void findUserByEmailTest() {

        when(userRepository.findByEmailAndDeletedIsFalse(EMAIL)).thenReturn(getMockUser());

        User actual = userService.findUserByEmail(EMAIL);

        assertEquals(getMockUser().get(), actual, "UserService.findUserByEmail cant pass the test");
    }

    private Optional<User> getMockUser() {

        User user = new User();
        user.setName("paul");
        user.setSurname("walker");
        user.setEmail(EMAIL);
        user.setPassword(PASSWORD);

        return Optional.of(user);
    }

    @Test
    void registerUserTest() {

        User user = getMockUser().get();

        when(userRepository.findByEmailAndDeletedIsFalse(EMAIL)).thenReturn(Optional.empty());
        when(passwordEncoder.encode(PASSWORD)).thenReturn(PASSWORD);
        when(userRepository.insert(user)).thenReturn(user);

        UserResponse actual = userService.registerUser(getRegisterRequest());

        assertEquals(getMockUserResponse(user), actual, "UserService.registerUser cant pass the test");
    }

    private UserResponse getMockUserResponse(User user) {
        UserResponse response = new UserResponse();

        response.setId(user.getId());
        response.setName(user.getName());
        response.setSurname(user.getSurname());
        response.setEmail(user.getEmail());
        response.setCreatedAt(user.getCreatedAt());
        response.setUpdatedAt(user.getUpdatedAt());

        return response;
    }

    private RegisterRequest getRegisterRequest() {

        RegisterRequest registerRequest = new RegisterRequest();
        registerRequest.setName("paul");
        registerRequest.setSurname("walker");
        registerRequest.setEmail(EMAIL);
        registerRequest.setPassword(PASSWORD);

        return registerRequest;
    }
}
