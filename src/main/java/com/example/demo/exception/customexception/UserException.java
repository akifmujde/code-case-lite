package com.example.demo.exception.customexception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class UserException extends RuntimeException{

    public UserException(String message) {
        super(message);
    }
}
