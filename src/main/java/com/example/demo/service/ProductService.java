package com.example.demo.service;

import com.example.demo.dto.request.CreateProductRequest;
import com.example.demo.dto.request.UpdateProductRequest;
import com.example.demo.dto.response.ProductResponse;
import com.example.demo.entity.Product;
import org.springframework.data.domain.Page;

public interface ProductService {

    ProductResponse createProduct(CreateProductRequest request);

    void deleteProduct(String productId);

    void updateProduct(String productId, UpdateProductRequest request);

    Page<Product> getProductBasedOnCategory(String categoryName, int page, int size);
}
