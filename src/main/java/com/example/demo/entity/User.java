package com.example.demo.entity;

import com.example.demo.entity.base.BaseEntity;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "user")
@Data
public class User extends BaseEntity {

    private String name;

    private String surname;

    private String email;

    private String password;
}
