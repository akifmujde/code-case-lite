package com.example.demo.configuration.jwt;

import io.jsonwebtoken.security.Keys;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.crypto.SecretKey;

@Configuration
@RequiredArgsConstructor
public class SecretKeyConf {

    private final JwtProperties props;

    @Bean
    public SecretKey getSecretKey() {

        byte[] bytesOfSecretKey = props.getSecretKey().getBytes();

        SecretKey secretKey = Keys.hmacShaKeyFor(bytesOfSecretKey);

        return secretKey;
    }
}
