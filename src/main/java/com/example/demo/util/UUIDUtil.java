package com.example.demo.util;

import java.util.UUID;

public class UUIDUtil {

    public static String getRandomUUD() {
        return UUID.randomUUID().toString();
    }
}
