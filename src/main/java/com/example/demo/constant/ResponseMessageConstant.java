package com.example.demo.constant;

public class ResponseMessageConstant {

    public static final String OK = "ok";
    public static final String ENTITY_CREATED = "%s successfully created";
}
