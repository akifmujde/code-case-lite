package com.example.demo.service;

import com.example.demo.entity.Role;
import com.example.demo.entity.User;
import com.example.demo.repository.RoleRepository;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.impl.RoleServiceImpl;
import com.example.demo.util.UUIDUtil;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class RoleServiceTest {

    @InjectMocks
    RoleServiceImpl roleService;

    @Mock
    RoleRepository roleRepository;

    @Mock
    UserRepository userRepository;

    private static final String USER_ID = UUIDUtil.getRandomUUD();

    @Test
    void findAllRoleOfUser() {

        when(userRepository.findById(USER_ID)).thenReturn(getMockUser());
        when(roleRepository.findAllByUserIdsContainsAndDeletedIsFalse(USER_ID)).thenReturn(getMockRoles());

        List<Role> allRoleOfUser = roleService.findAllRoleOfUser(USER_ID);

        assertEquals(getMockRoles(), allRoleOfUser, "RoleService.findAllRoleOfUser cant pass the test");
    }

    private List<Role> getMockRoles() {

        List<Role> roles = new LinkedList<>();
        return roles;
    }

    private Optional<User> getMockUser() {

        User user = new User();

        user.setName("paul");
        user.setSurname("walker");
        user.setEmail("paul.walker@exam.com");
        user.setPassword("Paul.Walker.2013");

        return Optional.of(user);
    }
}