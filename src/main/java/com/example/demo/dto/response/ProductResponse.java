package com.example.demo.dto.response;

import com.example.demo.dto.response.base.BaseResponse;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Map;

@Data
public class ProductResponse extends BaseResponse {

    private String name;

    private String barcode;

    private String cover;

    private BigDecimal price;

    private String categoryName;

    private Map<String, String> productProperties;
}
