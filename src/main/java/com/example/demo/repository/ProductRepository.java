package com.example.demo.repository;

import com.example.demo.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;
import java.util.Optional;

public interface ProductRepository extends MongoRepository<Product, String> {

    Optional<Product> findByIdAndDeletedIsFalse(String id);

    Optional<Product> findByBarcodeAndDeletedIsFalse(String barcode);

    List<Product> findAllByCategoryName(String categoryName);

    Page<Product> findAllByCategoryName(String categoryName, Pageable pageable);

    Page<Product> findAllByCategoryNameAndDeletedIsFalse(String categoryName, Pageable pageable);
}
