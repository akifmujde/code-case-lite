package com.example.demo.validation.entity;

import com.example.demo.constant.entity.UserConstant;
import com.example.demo.validation.StringValidation;

import java.util.function.Predicate;
import java.util.regex.Pattern;

public class UserValidation {

    public static Predicate<String> emailIsValid = email -> {

        if (StringValidation.isNotNullAndNotEmpty.test(email)){
            return Pattern.matches(UserConstant.EMAIL_VALIDATION, email);
        }

        return false;
    };
}
