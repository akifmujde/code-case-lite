package com.example.demo.util.time;

import org.joda.time.DateTime;

import java.util.Date;

public class DateUtil {

    public static Date getNowWithAfterDays(int days) {
        return DateTime.now().plusDays(days).toDate();
    }

    public static Date getNow() {
        return DateTime.now().toDate();
    }
}
