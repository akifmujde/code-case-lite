package com.example.demo.validation;

import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class StringValidationTest {

    private final String MESSAGE = "StringValidation.%s cant pass the test";

    @Test
    @Order(3)
    public void testIsNull() {

        String isNull = String.format(MESSAGE, "isNull");
        assertEquals(true, StringValidation.isNull.test(null), isNull);
        assertEquals(false, StringValidation.isNull.test(""), isNull);
        assertEquals(false, StringValidation.isNull.test("full"), isNull);
    }

    @Test
    @Order(2)
    public void testIsNotNull() {

        String isNotNull = String.format(MESSAGE, "isNotNull");
        assertEquals(false, StringValidation.isNotNull.test(null), isNotNull);
        assertEquals(true, StringValidation.isNotNull.test(""), isNotNull);
        assertEquals(true, StringValidation.isNotNull.test("full"), isNotNull);
    }

    @Test
    @Order(1)
    public void testIsNotNullAndNotEmpty() {

        String isNotNullAndNotEmpty = String.format(MESSAGE, "isNotNullAndNotEmpty");
        assertEquals(false, StringValidation.isNotNullAndNotEmpty.test(null), isNotNullAndNotEmpty);
        assertEquals(false, StringValidation.isNotNullAndNotEmpty.test(""), isNotNullAndNotEmpty);
        assertEquals(false, StringValidation.isNotNullAndNotEmpty.test("         "), isNotNullAndNotEmpty);
        assertEquals(true, StringValidation.isNotNullAndNotEmpty.test("full"), isNotNullAndNotEmpty);
        assertEquals(true, StringValidation.isNotNullAndNotEmpty.test("     full      "), isNotNullAndNotEmpty);
    }
}
