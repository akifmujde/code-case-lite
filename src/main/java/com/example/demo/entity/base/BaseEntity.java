package com.example.demo.entity.base;

import com.example.demo.util.time.DateUtil;
import com.example.demo.util.UUIDUtil;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;

import java.util.Date;

@Getter
public class BaseEntity {

    @Id
    @Setter
    private String id;

    private boolean deleted;

    @Setter
    private Date createdAt;

    private Date updatedAt;

    public BaseEntity() {

        this.id = UUIDUtil.getRandomUUD();
        this.deleted = false;

        Date now = DateUtil.getNow();

        this.createdAt = now;
        this.updatedAt = now;
    }

    public void setUpdatedAtToNow() {
        this.updatedAt = DateUtil.getNow();
    }

    public void deleteEntity() {
        this.deleted = true;
    }

    public void recoverEntity() {
        this.deleted = false;
    }
}
