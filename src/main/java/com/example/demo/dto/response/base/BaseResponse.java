package com.example.demo.dto.response.base;

import lombok.Data;

import java.util.Date;

@Data
public class BaseResponse {

    private String id;

    private Date createdAt;

    private Date updatedAt;
}
