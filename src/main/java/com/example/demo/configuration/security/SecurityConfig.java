package com.example.demo.configuration.security;

import com.example.demo.configuration.jwt.JwtProperties;
import com.example.demo.configuration.security.filter.LoginFilter;
import com.example.demo.configuration.security.filter.RequestFilter;
import com.example.demo.constant.security.PermittedPathConstant;
import com.example.demo.converter.AuthenticationConverter;
import com.example.demo.service.RoleService;
import com.example.demo.util.JwtUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
@RequiredArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final JwtProperties properties;
    private final JwtUtil jwt;
    private final DaoAuthenticationProvider daoAuthenticationProvider;
    private final RoleService roleService;
    private final AuthenticationConverter converter;

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http
                .csrf()
                    .disable()
                .sessionManagement()
                    .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                    .addFilter(new LoginFilter(jwt, properties, authenticationManager()))
                    .addFilterAfter(new RequestFilter(jwt, properties, roleService, converter),LoginFilter.class)
                .authorizeRequests()
                    .antMatchers(HttpMethod.POST, PermittedPathConstant.USER_PATH).permitAll()
                    .antMatchers(HttpMethod.GET, PermittedPathConstant.PRODUCT_PATH).permitAll()
                .anyRequest()
                .authenticated();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(daoAuthenticationProvider);
    }
}
