package com.example.demo.util;

import com.example.demo.util.JwtUtil;
import com.example.demo.util.UUIDUtil;
import io.jsonwebtoken.Jwts;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import javax.crypto.SecretKey;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class JwtUtilTest {

    @Autowired
    private SecretKey key;

    @Autowired
    private JwtUtil jwt;

    @Test
    @Order(1)
    public void testJwtGenerator() {

        String userId = UUIDUtil.getRandomUUD();

        String token = jwt.generateJwtForAuthentication(userId);

        String subject = Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token)
                .getBody()
                .getSubject();

        assertEquals(userId, subject, "Jwt generator can not pass test");
    }

    @Test
    @Order(2)
    public void testJwtExtractor() {

        String userId = UUIDUtil.getRandomUUD();

        String token = jwt.generateJwtForAuthentication(userId);

        String subject = jwt.extractJwtForAuthentication(token);

        assertEquals(userId, subject, "Jwt extractor can not pass test");
    }
}
