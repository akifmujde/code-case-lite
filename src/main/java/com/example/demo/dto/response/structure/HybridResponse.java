package com.example.demo.dto.response.structure;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Value;

@Value(staticConstructor = "of")
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class HybridResponse<T> {

    private T data;

    private T error;

    private String message;

    private Integer currentPage;

    private Long totalItems;

    private Integer totalPages;
}
