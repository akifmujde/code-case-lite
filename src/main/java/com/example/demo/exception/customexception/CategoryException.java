package com.example.demo.exception.customexception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class CategoryException extends RuntimeException {

    public CategoryException(String message) {
        super(message);
    }
}
