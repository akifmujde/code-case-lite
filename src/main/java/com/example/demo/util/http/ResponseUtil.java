package com.example.demo.util.http;

import com.example.demo.constant.ResponseMessageConstant;
import com.example.demo.dto.response.structure.HybridResponse;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

public class ResponseUtil {

    public static <T> ResponseEntity created(String message, T data) {
        String formattedMessage = String.format(ResponseMessageConstant.ENTITY_CREATED, message);
        return generateResponse(HttpStatus.CREATED, formattedMessage, data, null, null, null, null);
    }

    public static <T> ResponseEntity forbidden(String message) {
        return generateResponse(HttpStatus.FORBIDDEN, message, null, null, null, null, null);
    }

    public static <T> ResponseEntity badRequest(String message, T error) {
        return generateResponse(HttpStatus.BAD_REQUEST, message, null, error, null, null, null);
    }

    public static <T> ResponseEntity ok() {
        return generateResponse(HttpStatus.OK, ResponseMessageConstant.OK, null, null, null, null, null);
    }

    public static <T> ResponseEntity ok(Page<T> data) {
        List<T> content = data.getContent();
        return generateResponse(
                HttpStatus.OK,
                ResponseMessageConstant.OK,
                content,
                null,
                data.getNumber(),
                data.getTotalElements(),
                data.getTotalPages()
        );
    }

    public static <T> ResponseEntity noContent() {
        return generateResponse(HttpStatus.NO_CONTENT, null, null, null, null, null, null);
    }

    private static <T> ResponseEntity generateResponse(
            HttpStatus statusCode,
            String message,
            T data,
            T error,
            Integer currentPage,
            Long totalItems,
            Integer totalPages
    ) {

        HybridResponse<T> responseBody = HybridResponse.of(data, error, message, currentPage, totalItems, totalPages);

        ResponseEntity<HybridResponse<T>> response = new ResponseEntity<>(responseBody, statusCode);

        return response;
    }
}
