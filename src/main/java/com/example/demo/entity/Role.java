package com.example.demo.entity;

import com.example.demo.entity.base.BaseEntity;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Set;

@Document(collection = "role")
@Data
public class Role extends BaseEntity {

    private String roleName;

    private Set<String> permissions;

    private Set<String> userIds;
}
