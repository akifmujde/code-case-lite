package com.example.demo.configuration.security.filter;

import com.example.demo.configuration.jwt.JwtProperties;
import com.example.demo.dto.model.AuthenticationModel;
import com.example.demo.dto.request.LoginRequest;
import com.example.demo.dto.response.structure.HybridResponse;
import com.example.demo.entity.User;
import com.example.demo.util.JwtUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@RequiredArgsConstructor
public class LoginFilter extends UsernamePasswordAuthenticationFilter {

    private final JwtUtil jwt;
    private final JwtProperties props;
    private final AuthenticationManager authenticationManager;

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {

        LoginRequest loginRequest = null;
        try {
            loginRequest = new ObjectMapper().readValue(
                    request.getInputStream(),
                    LoginRequest.class
            );
        } catch (IOException e) {
            e.printStackTrace();
        }

        Authentication authentication = new UsernamePasswordAuthenticationToken(
                loginRequest.getEmail(),
                loginRequest.getPassword()
        );

        Authentication authenticate = authenticationManager.authenticate(authentication);

        return authenticate;
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain, Authentication authResult) throws IOException, ServletException {

        AuthenticationModel principal = (AuthenticationModel) authResult.getPrincipal();

        String userId = principal.getUserId();

        String token = jwt.generateJwtForAuthentication(userId);

        String headerValue = String.format("%s%s", props.getTokenPrefix(), token);

        response.setHeader(props.getAuthorizationHeader(), headerValue);
    }

    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest req, HttpServletResponse res, AuthenticationException failed) throws IOException, ServletException {

        String response = new Gson().toJson(HybridResponse.of(null, failed.getMessage(), "AuthenticationException",null,null,null));

        PrintWriter writer = res.getWriter();

        res.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        res.setContentType("application/json");
        res.setCharacterEncoding("UTF-8");
        writer.print(response);
        writer.flush();
    }
}
