package com.example.demo.util.time;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertNotEquals;

@SpringBootTest
public class DateUtilTest {

    @Autowired
    PasswordEncoder encoder;

    @Test
    public void testDateUtilNowFunction() throws InterruptedException {

        Date firstDate = DateUtil.getNow();

        TimeUnit.SECONDS.sleep(10);

        Date secondDate = DateUtil.getNow();

        assertNotEquals(firstDate, secondDate);
    }
}
