package com.example.demo.converter;

import com.example.demo.entity.Role;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.List;
import java.util.Set;

public interface AuthenticationConverter {

    Set<SimpleGrantedAuthority> convertUserRolesToSimpleGrantedAuthority(List<Role> userRoles);
}
