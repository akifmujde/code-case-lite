package com.example.demo.util;

import com.example.demo.configuration.jwt.JwtProperties;
import com.example.demo.util.time.DateUtil;
import io.jsonwebtoken.Jwts;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.crypto.SecretKey;

@Component
@RequiredArgsConstructor
public class JwtUtil {

    private final JwtProperties props;
    private final SecretKey secretKey;

    public String extractJwtForAuthentication(String token) {

        String userId = Jwts.parserBuilder()
                .setSigningKey(secretKey)
                .build()
                .parseClaimsJws(token)
                .getBody()
                .getSubject();

        return userId;
    }

    public String generateJwtForAuthentication(String userId) {

        Integer days = props.getTokenExpirationAfterDays();

        String token = Jwts.builder()
                .setSubject(userId)
                .setIssuedAt(DateUtil.getNow())
                .setExpiration(DateUtil.getNowWithAfterDays(days))
                .signWith(secretKey)
                .compact();

        return token;
    }
}
