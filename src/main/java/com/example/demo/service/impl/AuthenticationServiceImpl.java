package com.example.demo.service.impl;

import com.example.demo.converter.AuthenticationConverter;
import com.example.demo.dto.model.AuthenticationModel;
import com.example.demo.entity.Role;
import com.example.demo.entity.User;
import com.example.demo.service.AuthenticationService;
import com.example.demo.service.RoleService;
import com.example.demo.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class AuthenticationServiceImpl implements AuthenticationService {

    private final UserService userService;
    private final RoleService roleService;
    private final AuthenticationConverter converter;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

        UserDetails userDetails = getUserForAuthentication(email);

        return userDetails;
    }

    private UserDetails getUserForAuthentication(String email) {

        User user = userService.findUserByEmail(email);

        String userId = user.getId();
        String password = user.getPassword();

        Set<SimpleGrantedAuthority> userAuthorities = getUserAuthorities(userId);

        return new AuthenticationModel(userId, email, password, userAuthorities);
    }

    private Set<SimpleGrantedAuthority> getUserAuthorities(String userId) {

        List<Role> allRoleOfUser = roleService.findAllRoleOfUser(userId);
        Set<SimpleGrantedAuthority> authorities = converter.convertUserRolesToSimpleGrantedAuthority(allRoleOfUser);

        return authorities;
    }
}
