package com.example.demo.constant.security;

import com.example.demo.constant.ControllerConstant;

public class PermittedPathConstant {

    public static final String USER_PATH = String.format("%s%s", ControllerConstant.API_V1, ControllerConstant.USER_BASE_PATH);
    public static final String PRODUCT_PATH = String.format("%s%s", ControllerConstant.API_V1, ControllerConstant.PRODUCT_BASE_PATH);
}
