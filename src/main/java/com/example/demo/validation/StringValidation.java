package com.example.demo.validation;

import java.util.function.Predicate;

public class StringValidation {

    public static Predicate<String> isNull = value -> value == null;

    public static Predicate<String> isNotNull = value -> value != null;

    public static Predicate<String> isNotNullAndNotEmpty = value -> {

        if (isNull.test(value)) {
            return false;
        }

        if (value.trim().equals("")) {
            return false;
        }

        return true;
    };
}
