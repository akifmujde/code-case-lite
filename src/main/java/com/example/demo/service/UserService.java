package com.example.demo.service;

import com.example.demo.dto.request.RegisterRequest;
import com.example.demo.dto.response.UserResponse;
import com.example.demo.entity.User;

public interface UserService {

    User findUserByEmail(String email);

    UserResponse registerUser(RegisterRequest registerRequest);
}
