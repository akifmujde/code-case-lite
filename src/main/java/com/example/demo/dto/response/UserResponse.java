package com.example.demo.dto.response;

import com.example.demo.dto.response.base.BaseResponse;
import lombok.Data;

@Data
public class UserResponse extends BaseResponse {

    private String name;

    private String surname;

    private String email;
}
