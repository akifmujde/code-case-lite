package com.example.demo.entity;

import com.example.demo.entity.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "category")
@Data
public class Category extends BaseEntity {

    private String name;
}
