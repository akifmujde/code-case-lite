package com.example.demo.configuration.db;

import com.example.demo.constant.ControllerConstant;
import com.example.demo.entity.Category;
import com.example.demo.entity.Permission;
import com.example.demo.entity.Role;
import com.example.demo.entity.User;
import com.example.demo.repository.CategoryRepository;
import com.example.demo.repository.PermissionRepository;
import com.example.demo.repository.RoleRepository;
import com.example.demo.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.annotation.PostConstruct;
import java.util.*;

@Configuration
@RequiredArgsConstructor
public class InitialDataConf {

    private final PasswordEncoder passwordEncoder;
    private final RoleRepository roleRepository;
    private final UserRepository userRepository;
    private final CategoryRepository categoryRepository;
    private final PermissionRepository permissionRepository;

    @PostConstruct
    private void permissionInitialData() {
        permissionRepository.deleteAll();

        List<Permission> permissions = new LinkedList<>();

        for (String value : ControllerConstant.PERMISSIONS) {

            Permission permission = new Permission();

            permission.setPermissionName(value);

            permissions.add(permission);
        }

        permissionRepository.saveAll(permissions);
    }

    @PostConstruct
    private void userAndRoleInitialData() {
        userRepository.deleteAll();
        roleRepository.deleteAll();

        // admin

        User paulWalker = new User();

        paulWalker.setName("Paul");
        paulWalker.setSurname("Walker");
        paulWalker.setEmail("walker.paul@fastandfurious.com");
        paulWalker.setPassword(passwordEncoder.encode("Paul.Walker.2013"));

        userRepository.save(paulWalker);

        Set<String> userIds = new HashSet<>();
        userIds.add(paulWalker.getId());

        Set<String> permissions = new HashSet<>();
        permissions.addAll(Arrays.asList(ControllerConstant.PERMISSIONS));

        Role admin = new Role();
        admin.setRoleName("ADMIN");
        admin.setUserIds(userIds);
        admin.setPermissions(permissions);

        roleRepository.save(admin);

        // productAdder

        User vinDiesel = new User();

        vinDiesel.setName("Vin");
        vinDiesel.setSurname("Diesel");
        vinDiesel.setEmail("diesel.vin@fastandfurious.com");
        vinDiesel.setPassword(passwordEncoder.encode("Vin.Diesel.Current"));

        userRepository.save(vinDiesel);

        userIds.clear();
        userIds.add(vinDiesel.getId());

        permissions.clear();
        permissions.addAll(Arrays.asList(ControllerConstant.PERMISSIONS[0]));

        Role productAdder = new Role();
        productAdder.setRoleName("PRODUCT_ADDER");
        productAdder.setUserIds(userIds);
        productAdder.setPermissions(permissions);

        roleRepository.save(productAdder);
    }

    @PostConstruct
    private void categoryInitialData() {
        categoryRepository.deleteAll();

        String[] categories = {
                "Computer",
                "Dresses",
                "Watch",
                "Shoes",
                "Electronic",
                "Laptop"
        };

        List<Category> categoryList = new LinkedList<>();

        for (String value : categories) {
            Category category = new Category();
            category.setName(value);
            categoryList.add(category);
        }

        categoryRepository.saveAll(categoryList);
    }
}
