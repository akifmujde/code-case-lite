# code-case-lite

Welcome to Code Case lite. In this project we can provide to you a simple e commerce system. You can register and login our system and ofcourse create, update, delete, list product process we can provide. In product domain if you want to do something you must be authorized specific endpoint. Addittionally you have to provide jwt token every request header for our authentication system.

**--- For login request ---**
- Request
- Request Type    : POST
- Request URL     : /api/v1/login
- Request Body    : { "email": "", "password": "" }
- Response
- Successfully resfonse code = 200
- Response Headers: Authorization

So if your credentials are valid, you will like this respone get when you try to login our system. Next time you have to add Authorization header to your request headers.

**--- For register request ---**
- Request
- Request Type    : POST
- Request URL     : /api/v1/users
- Request Body    : { "name": "", "surname": "", "email": "", "password": "" }
- Response
- Successfully resfonse code = 201
- Response Body   : { "data" : { "id": "", "name": "", "surname": "", "email": "", "createdAt": "", "updatedAt": "" }, "message": "User successfully created" }

When you register this api our system create one user record with this credentials. But in this case you are a common user so you can only get product list based on category. You cant create, update and delete any product.

By the way dont worry. We have a 2 accounts for experience our system.

The following account has all the rights. I mean create, delete, update process.

email: walker.paul@fastandfurious.com
password: Paul.Walker.2013

The following account has only the "create-product".

email: diesel.vin@fastandfurious.com
password: Vin.Diesel.Current

**--- For Create Product ---**
- Request
- Request Type    : POST
- Request URL     : /api/v1/products
- Request header  : Authorization
- Request Body    : 
{
    "name" : "",
    "barcode" : "",
    "cover": "",
    "categoryName": "",
    "price": ,
    "productProperties": {
        "": ""
    }
}
- Response
- Successfully resfonse code = 201
- Response Body   :
{
    "data": {
        "id": "",
        "createdAt": ,
        "updatedAt": ,
        "name": "",
        "barcode": "",
        "cover": "",
        "price": ,
        "categoryName": "",
        "productProperties": {
            "": ""
        }
    },
    "message": "Product successfully created"
}

If you want to create a product, you have to provide this request properties and your account must had create-product permission. But we have a some rules for product creation. All of this properties are mandatory. After that you have to provide unique barcode and still exist category. If you pass these rules your product will be add our db.


**--- For delete product ---**
- Request
- Request Type    : Delete
- Request URL     : /api/v1/products
- Path variable   : ProductID
- Request header  : Authorization
- Response
- Successfully resfonse code = 204

For delete product endpoint you just give the one exist product id and you need the delete-product permission



**--- For update product ---**
- Request
- Request Type    : PUT
- Request URL     : /api/v1/products
- Path variable   : ProductID
- Request header  : Authorization
- Request Body    : 
{
    "name" : "",
    "barcode" : "",
    "cover": "",
    "categoryName": "",
    "price": ,
    "productProperties": {
        "": ""
    }
}
- Response
- Successfully resfonse code = 200
- Response Body   : {
    "message": "ok"
}

For update product endpoint you have to provide one exist product id and which areas of the product you want to update, you must give those fields. By the way barcode is unique and category field will be exist in our categories. Additionally your account must to have update-product permission for this request.



**--- For get product based on category ---**
- Request
- Request Type    : GET
- Request URL     : /api/v1/products?categoryName=Laptop&page=0&size=1
- Request Param   : categoryName
- Request Param   : page
- Request Param   : size
- Request header  : Authorization
- Response
- Successfully resfonse code = 200
- Response Body   : {
    "data": [
        {
            "id": "",
            "deleted": true,
            "createdAt": ,
            "updatedAt": ,
            "name": "",
            "barcode": "",
            "cover": "",
            "price": ,
            "categoryName": "",
            "productProperties": {
            }
        }
    ],
    "message": "ok",
    "currentPage": 0,
    "totalItems": ,
    "totalPages": 
}


This request for list products. You can filter based on category field. This field is mandatory and it will be exist on db. page and size provide to you paggination.







