package com.example.demo.dto.request;

import com.example.demo.constant.entity.UserConstant;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Data
public class RegisterRequest {

    @NotBlank(message = "Name field is required")
    private String name;

    @NotBlank(message = "surname field is required")
    private String surname;

    @NotBlank(message = "Email field is required")
    @Email(regexp = UserConstant.EMAIL_VALIDATION, message = "Email is not valid")
    private String email;

    @NotBlank(message = "Password field is required")
    @Pattern(regexp = UserConstant.PASSWORD_VALIDATION, message = "Password is not valid")
    private String password;
}
