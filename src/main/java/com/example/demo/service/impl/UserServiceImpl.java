package com.example.demo.service.impl;

import com.example.demo.dto.request.RegisterRequest;
import com.example.demo.dto.response.UserResponse;
import com.example.demo.entity.User;
import com.example.demo.mapper.UserMapper;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.UserService;
import com.example.demo.exception.management.entity.UserThrower;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    UserMapper userMapper = UserMapper.INSTANCE;

    @Override
    public User findUserByEmail(String email) {

        UserThrower.checkEmailIsValid(email);

        Optional<User> user = userRepository.findByEmailAndDeletedIsFalse(email);

        UserThrower.checkUserIsExist(user);

        return user.get();
    }


    @Override
    public UserResponse registerUser(RegisterRequest registerRequest) {

        checkEmailIsAlreadyUsing(registerRequest.getEmail());

        User userForRegistration = getUserForRegistration(registerRequest);

        User savedUser = userRepository.insert(userForRegistration);

        return userMapper.userToUserResponse(savedUser);
    }

    private User getUserForRegistration(RegisterRequest registerRequest) {

        registerRequest.setPassword(encodePassword(registerRequest.getPassword()));
        User user = userMapper.registerRequestToUser(registerRequest);

        return user;
    }

    private void checkEmailIsAlreadyUsing(String email) {

        Optional<User> user = userRepository.findByEmailAndDeletedIsFalse(email);
        UserThrower.checkUserIsExistForRegistration(user);
    }

    private String encodePassword(String password) {
        return passwordEncoder.encode(password);
    }
}
