package com.example.demo.repository;

import com.example.demo.entity.Role;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface RoleRepository extends MongoRepository<Role, String> {

    List<Role> findAllByUserIdsContainsAndDeletedIsFalse(String userId);
}
