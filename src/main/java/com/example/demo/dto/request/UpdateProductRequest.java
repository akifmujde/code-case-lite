package com.example.demo.dto.request;

import lombok.Data;

import javax.validation.constraints.Min;
import java.math.BigDecimal;
import java.util.Map;

@Data
public class UpdateProductRequest {

    private String name;

    private String barcode;

    private String cover;

    private String categoryName;

    @Min(value = 0, message = "Price must be greater than zero")
    private BigDecimal price;

    private Map<String, String> productProperties;
}
