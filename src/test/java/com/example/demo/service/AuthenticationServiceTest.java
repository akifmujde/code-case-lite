package com.example.demo.service;

import com.example.demo.converter.AuthenticationConverter;
import com.example.demo.dto.model.AuthenticationModel;
import com.example.demo.entity.Role;
import com.example.demo.entity.User;
import com.example.demo.service.impl.AuthenticationServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
public class AuthenticationServiceTest {

    @InjectMocks
    AuthenticationServiceImpl authenticationService;

    @Mock
    UserService userService;

    @Mock
    RoleService roleService;

    @Mock
    AuthenticationConverter converter;

    private final static String PASSWORD = "Paul.Walker.2013";
    private final static String EMAIL = "paul.walker@exam.com";

    @Test
    void loadUserByUsername() {

        User mockUser = getMockUser();
        String userId = mockUser.getId();
        when(userService.findUserByEmail(EMAIL)).thenReturn(mockUser);

        List<Role> mockRoleList = getMockRoleList(userId);
        when(roleService.findAllRoleOfUser(userId)).thenReturn(mockRoleList);

        Set<SimpleGrantedAuthority> mockGrantedAuthorities = getMockGrantedAuthorities();
        when(converter.convertUserRolesToSimpleGrantedAuthority(mockRoleList)).thenReturn(mockGrantedAuthorities);

        UserDetails userDetails = authenticationService.loadUserByUsername(EMAIL);

        UserDetails mockUserDetails = getMockUserDetails(userId, mockGrantedAuthorities);
        assertEquals(mockUserDetails, userDetails, "AuthenticationService:loadUserByUsername cant pass the test");
    }

    private UserDetails getMockUserDetails(String userId, Set<SimpleGrantedAuthority> authorities) {

        return new AuthenticationModel(userId, EMAIL, PASSWORD, authorities);
    }

    private Set<SimpleGrantedAuthority> getMockGrantedAuthorities() {

        Set<SimpleGrantedAuthority> authorities = new HashSet<>();

        authorities.add(new SimpleGrantedAuthority("authorize-user"));
        authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));

        return authorities;
    }

    private List<Role> getMockRoleList(String userId) {
        List<Role> roleList = new LinkedList<>();

        Set<String> permissions = new HashSet<>();
        permissions.add("authorize-user");

        Set<String> userIds = new HashSet<>();
        permissions.add(userId);

        Role role = new Role();

        role.setRoleName("ADMIN");
        role.setPermissions(permissions);
        role.setUserIds(userIds);

        roleList.add(role);

        return roleList;
    }

    private User getMockUser() {

        User user = new User();

        user.setName("paul");
        user.setSurname("walker");
        user.setEmail(EMAIL);
        user.setPassword(PASSWORD);

        return user;
    }
}
