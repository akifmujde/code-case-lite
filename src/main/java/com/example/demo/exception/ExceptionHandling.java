package com.example.demo.exception;

import com.example.demo.exception.customexception.CategoryException;
import com.example.demo.exception.customexception.ProductException;
import com.example.demo.exception.customexception.UserException;
import com.example.demo.util.http.ResponseUtil;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import io.jsonwebtoken.security.SignatureException;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.validation.ConstraintViolationException;
import java.util.Set;
import java.util.stream.Collectors;

@ControllerAdvice
public class ExceptionHandling {

    @ExceptionHandler(
            value = {
                    RuntimeException.class,
                    UserException.class,
                    ProductException.class,
                    CategoryException.class
            }
    )
    public ResponseEntity manageRuntimeException(RuntimeException e) {

        String message = e.getMessage();
        return ResponseUtil.badRequest(message, null);
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    ResponseEntity manageHttpMessageNotReadableException(HttpMessageNotReadableException e) {

        return ResponseUtil.badRequest("RequestBodyException", e.getMessage());
    }

    @ExceptionHandler(BadCredentialsException.class)
    public ResponseEntity manageBadCredentialsException(BadCredentialsException e) {

        String message = e.getMessage();
        return ResponseUtil.badRequest("BadCredentialsException", message);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity manageConstraintViolationException(ConstraintViolationException e) {

        String message = e.getMessage().split(":")[1].trim();
        return ResponseUtil.badRequest("ConstraintViolationException", message);
    }

    @ExceptionHandler(SignatureException.class)
    public ResponseEntity manageSignatureException(SignatureException e) {

        String message = e.getMessage();
        return ResponseUtil.badRequest("SignatureException", message);
    }

    @ExceptionHandler(AccessDeniedException.class)
    public ResponseEntity manageAccessDeniedException(AccessDeniedException e) {

        String message = e.getMessage();
        return ResponseUtil.forbidden(message);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity manageMethodArgumentNotValidException(MethodArgumentNotValidException e) {

        Set<String> errorDetails = e.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(
                        exception -> exception.getDefaultMessage()
                )
                .collect(Collectors.toSet());

        String message = "Request fields are not valid";

        return ResponseUtil.badRequest(message, errorDetails);
    }
}
