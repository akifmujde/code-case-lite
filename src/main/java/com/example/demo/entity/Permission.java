package com.example.demo.entity;

import com.example.demo.entity.base.BaseEntity;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "permission")
@Data
public class Permission extends BaseEntity {

    private String permissionName;
}
