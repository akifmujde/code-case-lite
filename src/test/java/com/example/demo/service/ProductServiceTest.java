package com.example.demo.service;

import com.example.demo.dto.request.CreateProductRequest;
import com.example.demo.dto.request.UpdateProductRequest;
import com.example.demo.dto.response.ProductResponse;
import com.example.demo.entity.Category;
import com.example.demo.entity.Product;
import com.example.demo.repository.CategoryRepository;
import com.example.demo.repository.ProductRepository;
import com.example.demo.service.impl.ProductServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ProductServiceTest {

    @InjectMocks
    ProductServiceImpl productService;

    @Mock
    ProductRepository productRepository;

    @Mock
    CategoryRepository categoryRepository;

    @Test
    void deleteProduct() {
        Product mockProduct = getMockProduct();

        String id = mockProduct.getId();
        when(productRepository.findByIdAndDeletedIsFalse(id)).thenReturn(Optional.of(mockProduct));

        mockProduct.deleteEntity();
        when(productRepository.save(mockProduct)).thenReturn(mockProduct);

        productService.deleteProduct(id);
    }

    @Test
    void createProduct() {

        when(categoryRepository.findByNameAndDeletedIsFalse("CATEGORY_NAME")).thenReturn(getMockCategory());

        Product mockProduct = getMockProduct();
        when(productRepository.insert(mockProduct)).thenReturn(mockProduct);

        when(productRepository.findByBarcodeAndDeletedIsFalse(mockProduct.getBarcode())).thenReturn(Optional.empty());

        ProductResponse response = productService.createProduct(getMockCreateRequest());

        assertEquals(mockProductResponse(mockProduct), response, "ProductService.createProduct cant pass the test");
    }

    private ProductResponse mockProductResponse(Product product) {

        ProductResponse res = new ProductResponse();

        res.setId(product.getId());
        res.setBarcode(product.getBarcode());
        res.setCategoryName(product.getCategoryName());
        res.setCover(product.getCover());
        res.setName(product.getName());
        res.setPrice(product.getPrice());
        res.setProductProperties(product.getProductProperties());
        res.setCreatedAt(product.getCreatedAt());
        res.setUpdatedAt(product.getUpdatedAt());

        return res;
    }


    @Test
    void updateProduct() {

        Product mockProduct = getMockProduct();

        String id = mockProduct.getId();
        when(productRepository.findByIdAndDeletedIsFalse(id)).thenReturn(Optional.of(mockProduct));
        when(productRepository.findByBarcodeAndDeletedIsFalse(mockProduct.getBarcode())).thenReturn(Optional.empty());
        when(categoryRepository.findByNameAndDeletedIsFalse(mockProduct.getCategoryName())).thenReturn(getMockCategory());
        when(productRepository.save(mockProduct)).thenReturn(mockProduct);

        productService.updateProduct(id, getMockUpdateRequest());
    }

    private Product getMockProduct() {

        Product product = new Product();

        Map<String, String> props = new HashMap<>();
        props.put("Cpu", "OctaCore");

        product.setName("Apple");
        product.setBarcode("11111");
        product.setCategoryName("CATEGORY_NAME");
        product.setCover("A13SAD123GKHASDXAWTH");
        product.setPrice(BigDecimal.TEN);
        product.setProductProperties(props);

        return product;
    }

    private UpdateProductRequest getMockUpdateRequest() {

        UpdateProductRequest request = new UpdateProductRequest();

        request.setName("Apple");
        request.setBarcode("11111");
        request.setCategoryName("CATEGORY_NAME");

        return request;
    }

    private CreateProductRequest getMockCreateRequest() {

        Map<String, String> props = new HashMap<>();

        props.put("Cpu", "OctaCore");

        CreateProductRequest createProductRequest = new CreateProductRequest();

        createProductRequest.setName("Apple");
        createProductRequest.setBarcode("11111");
        createProductRequest.setCategoryName("CATEGORY_NAME");
        createProductRequest.setCover("A13SAD123GKHASDXAWTH");
        createProductRequest.setPrice(BigDecimal.TEN);
        createProductRequest.setProductProperties(props);

        return createProductRequest;
    }

    private Optional<Category> getMockCategory() {

        Category category = new Category();
        category.setName("CATEGORY_NAME");

        return Optional.of(category);
    }
}
