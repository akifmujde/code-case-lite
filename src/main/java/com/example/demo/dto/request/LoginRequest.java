package com.example.demo.dto.request;

import com.example.demo.constant.entity.UserConstant;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Data
public class LoginRequest {

    @NotBlank(message = "Email field is required.")
    @Email(message = "Email is not valid.")
    private String email;


    @NotBlank(message = "Password field is required.")
    @Pattern(regexp = UserConstant.PASSWORD_VALIDATION, message = "Password is not valid.")
    private String password;
}
