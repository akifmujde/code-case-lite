package com.example.demo.service.impl;

import com.example.demo.dto.request.CreateProductRequest;
import com.example.demo.dto.request.UpdateProductRequest;
import com.example.demo.dto.response.ProductResponse;
import com.example.demo.entity.Category;
import com.example.demo.entity.Product;
import com.example.demo.exception.management.entity.CategoryThrower;
import com.example.demo.exception.management.entity.ProductThrower;
import com.example.demo.mapper.ProductMapper;
import com.example.demo.repository.CategoryRepository;
import com.example.demo.repository.ProductRepository;
import com.example.demo.service.ProductService;
import com.example.demo.validation.StringValidation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final CategoryRepository categoryRepository;

    ProductMapper productMapper = ProductMapper.INSTANCE;

    @Override
    public ProductResponse createProduct(CreateProductRequest request) {

        validateRequest(request.getBarcode(), request.getCategoryName());

        Product product = insertProduct(request);
        return productMapper.productToProductResponse(product);
    }

    private Product insertProduct(CreateProductRequest request) {
        Product product = productMapper.createProductRequestToProduct(request);

        Product savedProduct = productRepository.insert(product);

        return savedProduct;
    }

    private void validateRequest(String barcode, String categoryName) {

        if (StringValidation.isNotNullAndNotEmpty.test(barcode)) {

            validateBarcode(barcode);
        }

        if (StringValidation.isNotNullAndNotEmpty.test(categoryName)) {

            validateCategory(categoryName);
        }
    }

    private void validateCategory(String categoryName) {
        Optional<Category> category = categoryRepository.findByNameAndDeletedIsFalse(categoryName);
        CategoryThrower.checkCategoryIsExist(category);
    }

    private void validateBarcode(String barcode) {
        Optional<Product> product = productRepository.findByBarcodeAndDeletedIsFalse(barcode);
        ProductThrower.checkBarcodeIsAlreadyExist(product);
    }

    @Override
    public void deleteProduct(String productId) {

        Product product = getProductIsExist(productId);
        deleteProduct(product);
    }

    private Product getProductIsExist(String productId) {

        Optional<Product> product = productRepository.findByIdAndDeletedIsFalse(productId);
        ProductThrower.checkProductIsExist(product);
        return product.get();
    }

    private void deleteProduct(Product product) {

        product.deleteEntity();
        productRepository.save(product);
    }

    @Override
    public void updateProduct(String productId, UpdateProductRequest request) {

        Product product = getProductIsExist(productId);

        if (!product.getBarcode().equals(request.getBarcode())) {
            validateRequest(request.getBarcode(), request.getCategoryName());
        }
        else{
            validateCategory(request.getCategoryName());
        }

        persistProductChanges(product, request);
    }

    private void persistProductChanges(Product product, UpdateProductRequest request) {

        productMapper.updateProductRequestToProduct(product, request);
        productRepository.save(product);
    }

    @Override
    public Page<Product> getProductBasedOnCategory(String categoryName, int page, int size) {

        validateCategory(categoryName);

        Page<Product> allByCategoryName = productRepository.findAllByCategoryNameAndDeletedIsFalse(categoryName, PageRequest.of(page, size));

        return allByCategoryName;
    }
}
