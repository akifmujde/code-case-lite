package com.example.demo.entity;

import com.example.demo.entity.base.BaseEntity;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import java.math.BigDecimal;
import java.util.Map;

@Document
@Data
public class Product extends BaseEntity {

    private String name;

    private String barcode;

    private String cover;

    private BigDecimal price;

    private String categoryName;

    private Map<String, String> productProperties;
}
