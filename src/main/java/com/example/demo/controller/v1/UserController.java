package com.example.demo.controller.v1;

import com.example.demo.configuration.apiversion.ApiVersion;
import com.example.demo.constant.ControllerConstant;
import com.example.demo.dto.request.RegisterRequest;
import com.example.demo.dto.response.UserResponse;
import com.example.demo.service.UserService;
import com.example.demo.util.http.ResponseUtil;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@ApiVersion(1)
@RequestMapping(value = ControllerConstant.USER_BASE_PATH)
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @PostMapping
    public ResponseEntity registerUser(@Valid @RequestBody RegisterRequest registerRequest) {
        UserResponse user = userService.registerUser(registerRequest);
        return ResponseUtil.created("User", user);
    }
}
