package com.example.demo.exception.management.entity;

import com.example.demo.entity.Category;
import com.example.demo.exception.customexception.CategoryException;

import java.util.Optional;

public class CategoryThrower {

    public static void checkCategoryIsExist(Optional<Category> category) {

        if (!category.isPresent()) {
            throw new CategoryException("Given category name is not exist");
        }
    }
}
