package com.example.demo.validation;

import com.example.demo.validation.entity.UserValidation;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.assertEquals;


@SpringBootTest
public class UserValidationTest {

    @Test
    public void testEmailIsValid() {

        String email = "mujde.akif@gmail.com";
        String message = "UserValidation.emailIsValid cant pass test";

        assertEquals(true, UserValidation.emailIsValid.test(email), message);
        assertEquals(false, UserValidation.emailIsValid.test(""), message);
        assertEquals(false, UserValidation.emailIsValid.test(null), message);
    }

}
