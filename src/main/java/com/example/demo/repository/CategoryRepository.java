package com.example.demo.repository;

import com.example.demo.entity.Category;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface CategoryRepository extends MongoRepository<Category, String> {

    Optional<Category> findByNameAndDeletedIsFalse(String categoryName);
}
