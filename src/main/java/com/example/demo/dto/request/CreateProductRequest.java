package com.example.demo.dto.request;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Map;

@Data
public class CreateProductRequest {

    @NotBlank(message = "Name field is required")
    private String name;

    @NotBlank(message = "Barcode field is required")
    private String barcode;

    @NotBlank(message = "Cover field is required")
    private String cover;

    @NotBlank(message = "Category name field is required")
    private String categoryName;

    @Min(value = 0, message = "Price must be greater than zero")
    private BigDecimal price;

    @NotNull(message = "Product properties field is required")
    private Map<String, String> productProperties;
}
