package com.example.demo.constant;

public class ControllerConstant {

    public static final String API_V1 = "/api/v1";

    public static final String USER_BASE_PATH = "/users";

    public static final String PRODUCT_BASE_PATH = "/products";

    public static final String[] PERMISSIONS = {
            "create-product",
            "delete-product",
            "update-product"
    };
}
