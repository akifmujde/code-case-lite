package com.example.demo.configuration.security.filter;

import com.example.demo.configuration.jwt.JwtProperties;
import com.example.demo.converter.AuthenticationConverter;
import com.example.demo.entity.Role;
import com.example.demo.service.RoleService;
import com.example.demo.util.JwtUtil;
import com.example.demo.validation.StringValidation;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Set;

@RequiredArgsConstructor
public class RequestFilter extends OncePerRequestFilter {

    private final JwtUtil jwt;
    private final JwtProperties props;
    private final RoleService roleService;
    private final AuthenticationConverter converter;

    @Override
    protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws ServletException, IOException {

        String authorizationHeader = req.getHeader(props.getAuthorizationHeader());

        if (!authenticationHeaderIsValid(authorizationHeader)) {
            chain.doFilter(req, res);
            return;
        }

        String userId = getUserIdFromRequestHeaderValue(authorizationHeader);

        UsernamePasswordAuthenticationToken contextParams = getSecurityContextParams(userId);

        SecurityContextHolder.getContext().setAuthentication(contextParams);

        chain.doFilter(req, res);
    }

    private UsernamePasswordAuthenticationToken getSecurityContextParams(String userId) {

        Set<SimpleGrantedAuthority> userAuthorities = getUserAuthorities(userId);

        return new UsernamePasswordAuthenticationToken(
                userId,
                null,
                userAuthorities
        );
    }

    private String getUserIdFromRequestHeaderValue(String value) {

        String pureJwtToken = value.replace(props.getTokenPrefix(), "");
        String userId = jwt.extractJwtForAuthentication(pureJwtToken);

        return userId;
    }

    private Set<SimpleGrantedAuthority> getUserAuthorities(String userId) {
        List<Role> allRoleOfUser = roleService.findAllRoleOfUser(userId);
        Set<SimpleGrantedAuthority> authorities = converter.convertUserRolesToSimpleGrantedAuthority(allRoleOfUser);
        return authorities;
    }

    private boolean authenticationHeaderIsValid(String value) {

        if (StringValidation.isNotNullAndNotEmpty.test(value)) {
            return value.startsWith(props.getTokenPrefix());
        }

        return false;
    }
}
