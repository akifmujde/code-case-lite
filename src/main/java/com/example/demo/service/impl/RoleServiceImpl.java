package com.example.demo.service.impl;

import com.example.demo.entity.Role;
import com.example.demo.entity.User;
import com.example.demo.exception.management.entity.UserThrower;
import com.example.demo.repository.RoleRepository;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.RoleService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;
    private final UserRepository userRepository;

    @Override
    public List<Role> findAllRoleOfUser(String userId) {

        checkUserByUserId(userId);

        List<Role> userRoles = roleRepository.findAllByUserIdsContainsAndDeletedIsFalse(userId);

        return userRoles;
    }

    private void checkUserByUserId(String userId) {

        Optional<User> user = userRepository.findById(userId);

        UserThrower.checkUserIsExist(user);
    }
}
