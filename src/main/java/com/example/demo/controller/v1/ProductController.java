package com.example.demo.controller.v1;

import com.example.demo.configuration.apiversion.ApiVersion;
import com.example.demo.constant.ControllerConstant;
import com.example.demo.dto.request.CreateProductRequest;
import com.example.demo.dto.request.UpdateProductRequest;
import com.example.demo.dto.response.ProductResponse;
import com.example.demo.entity.Product;
import com.example.demo.service.ProductService;
import com.example.demo.util.http.ResponseUtil;
import lombok.RequiredArgsConstructor;
import org.hibernate.validator.constraints.Range;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

@RestController
@ApiVersion(1)
@RequiredArgsConstructor
@RequestMapping(value = ControllerConstant.PRODUCT_BASE_PATH)
@Validated
public class ProductController {

    private final ProductService productService;

    @PreAuthorize("hasAuthority('create-product')")
    @PostMapping
    public ResponseEntity createProduct(@Valid @RequestBody CreateProductRequest request) {
        ProductResponse product = productService.createProduct(request);
        return ResponseUtil.created("Product", product);
    }

    @PreAuthorize("hasAuthority('delete-product')")
    @DeleteMapping(value = "/{productId}")
    public ResponseEntity deleteProduct(
            @PathVariable @NotBlank(message = "Product id is required for delete product process")
                    String productId
    ) {
        productService.deleteProduct(productId);
        return ResponseUtil.noContent();
    }

    @PreAuthorize("hasAuthority('update-product')")
    @PutMapping(value = "/{productId}")
    public ResponseEntity updateProduct(
            @PathVariable @NotBlank(message = "Product id is required for delete product process")
                    String productId,
            @Valid @RequestBody UpdateProductRequest request
    ) {
        productService.updateProduct(productId, request);
        return ResponseUtil.ok();
    }

    @GetMapping
    public ResponseEntity getProductBasedOnCategory(
            @RequestParam @NotBlank(message = "Category field is required")
                    String categoryName,
            @RequestParam(defaultValue = "0") @Min(value = 0, message = "The number of pages must be greater than zero")
                    Integer page,
            @RequestParam(defaultValue = "5") @Range(min = 1, max = 10, message = "Size must be between 1 and 10")
                    Integer size
    ) {
        Page<Product> productBasedOnCategory = productService.getProductBasedOnCategory(categoryName, page, size);
        return ResponseUtil.ok(productBasedOnCategory);
    }
}
