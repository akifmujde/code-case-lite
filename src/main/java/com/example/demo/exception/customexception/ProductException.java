package com.example.demo.exception.customexception;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class ProductException extends RuntimeException {

    public ProductException(String message) {
        super(message);
    }
}
