package com.example.demo.converter.impl;

import com.example.demo.converter.AuthenticationConverter;
import com.example.demo.entity.Role;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
public class AuthenticationConverterImpl implements AuthenticationConverter {

    private final String DEFAULT_ROLE_PREFIX = "ROLE_";

    @Override
    public Set<SimpleGrantedAuthority> convertUserRolesToSimpleGrantedAuthority(List<Role> userRoles) {

        Set<SimpleGrantedAuthority> authorities = new HashSet<>();

        for (Role role : userRoles) {

            Set<SimpleGrantedAuthority> simplePerm = role.getPermissions()
                    .stream()
                    .map(perm -> new SimpleGrantedAuthority(perm))
                    .collect(Collectors.toSet());

            authorities.addAll(simplePerm);

            authorities.add(new SimpleGrantedAuthority(String.format("%s%s", DEFAULT_ROLE_PREFIX, role.getRoleName())));
        }

        return authorities;
    }
}
