package com.example.demo.exception.management.entity;

import com.example.demo.entity.Product;
import com.example.demo.exception.customexception.ProductException;
import com.example.demo.exception.customexception.UserException;

import java.util.Optional;

public class ProductThrower {

    public static void checkBarcodeIsAlreadyExist(Optional<Product> product) {

        if (product.isPresent()) {
            throw new ProductException("Given barcode already exist");
        }
    }

    public static void checkProductIsExist(Optional<Product> product) {

        if (!product.isPresent()) {
            throw new UserException("Given product not found");
        }
    }
}
